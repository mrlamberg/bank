package ru.parma.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.javamoney.moneta.Money;
import ru.parma.test.exception.InsufficientFundsException;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;

    @Columns(columns = {@Column(name = "currency"), @Column(name = "balance")})
    @Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money balance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    public Account(Client client, BigDecimal startBalance) {
        this.client = client;
        this.balance = Money.of(startBalance, "RUB");
    }

    public Account() {
    }

    public void increaseBalance(Money amount) {
        balance = balance.add(amount);
    }

    public void decreaseBalance(Money amount) {
        balance = balance.subtract(amount);
    }

    public void checkBalanceNotNegative() {
        if (balance.isNegative()) {
            throw new InsufficientFundsException("Insufficient funds in the bank account balance");
        }
    }

    @JsonProperty
    public BigDecimal getBalanceAmount() {
        return balance.getNumberStripped();
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonProperty
    public Money getBalance() {
        return balance;
    }

    @JsonIgnore
    public Client getClient() {
        return client;
    }
}
