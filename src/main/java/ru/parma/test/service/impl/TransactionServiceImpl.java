package ru.parma.test.service.impl;

import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.parma.test.entity.*;
import ru.parma.test.exception.AccountNotFoundException;
import ru.parma.test.filter.TransactionFilter;
import ru.parma.test.repository.AccountBalanceOperationRepository;
import ru.parma.test.repository.AccountRepository;
import ru.parma.test.service.TransactionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class TransactionServiceImpl implements TransactionService {

    private final AccountRepository accountRepository;
    private final AccountBalanceOperationRepository accountBalanceOperationRepository;
    private final AccountBalanceOperationFactory accountBalanceOperationFactory;
    private final TransactionFactory transactionFactory;
    private final EntityManager entityManager;
    private final CriteriaBuilder criteriaBuilder;

    @Autowired
    public TransactionServiceImpl(AccountRepository accountRepository,
                                  AccountBalanceOperationRepository accountBalanceOperationRepository,
                                  AccountBalanceOperationFactory accountBalanceOperationFactory,
                                  TransactionFactory transactionFactory, EntityManagerFactory entityManagerFactory) {
        this.accountRepository = accountRepository;
        this.accountBalanceOperationRepository = accountBalanceOperationRepository;
        this.accountBalanceOperationFactory = accountBalanceOperationFactory;
        this.transactionFactory = transactionFactory;
        this.entityManager = entityManagerFactory.createEntityManager();
        this.criteriaBuilder = this.entityManager.getCriteriaBuilder();


    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Transaction transferBetweenAccounts(Long fromAccountId, Long toAccountId,
                                               Money amount) {

        List<Account> accounts = accountRepository.findByIdIn(
                Arrays.asList(fromAccountId, toAccountId));

        Account fromBankAccount = getBankAccountByNumber(accounts, fromAccountId);
        Account toBankAccount = getBankAccountByNumber(accounts, toAccountId);

        fromBankAccount.decreaseBalance(amount);
        fromBankAccount.checkBalanceNotNegative();
        toBankAccount.increaseBalance(amount);

        Transaction transaction = transactionFactory.createTransaction(fromBankAccount, toBankAccount, amount);

        AccountBalanceOperation fromBankAccountBalanceOperation =
                accountBalanceOperationFactory.createDebitAccountBalanceOperation(transaction);
        AccountBalanceOperation toBankAccountBalanceOperation =
                accountBalanceOperationFactory.createCreditAccountBalanceOperation(transaction);

        accountBalanceOperationRepository.save(fromBankAccountBalanceOperation);
        accountBalanceOperationRepository.save(toBankAccountBalanceOperation);

        return transaction;
    }

    @Override
    public List<AccountBalanceOperation> find(TransactionFilter filter) throws ParseException {

        CriteriaQuery<AccountBalanceOperation> accountBalanceOperationCriteriaQuery = this.criteriaBuilder.createQuery(AccountBalanceOperation.class);
        Root<AccountBalanceOperation> accountBalanceOperationRoot = accountBalanceOperationCriteriaQuery.from(AccountBalanceOperation.class);
        Join<AccountBalanceOperation, Client> accountBalanceOperationAccountJoin = accountBalanceOperationRoot.join("account").join("client");

        List<Predicate> list = new ArrayList<>();
        if (filter.getName() != null && !filter.getName().isEmpty()) {
            list.add(this.criteriaBuilder.equal(accountBalanceOperationAccountJoin.get("name"), filter.getName()));
        }

        if (filter.getDateFrom() != null && filter.getDateTo() != null) {
            list.add(this.criteriaBuilder.between(accountBalanceOperationRoot.get("createTime"), filter.getDateFrom(), filter.getDateTo()));
        }
        accountBalanceOperationCriteriaQuery
                .select(accountBalanceOperationRoot)
                .orderBy(criteriaBuilder.asc(accountBalanceOperationRoot.get("createTime")));
        if (list.size() > 0) {
            accountBalanceOperationCriteriaQuery
                    .where(this.criteriaBuilder
                            .and((Predicate[]) list.toArray(new Predicate[0]))
                    );
        }
        TypedQuery<AccountBalanceOperation> typedQuery = entityManager.createQuery(accountBalanceOperationCriteriaQuery);
        typedQuery.setFirstResult(filter.getPage() * filter.getSize());
        typedQuery.setMaxResults(filter.getSize());
        return typedQuery.getResultList();
    }

    private Account getBankAccountByNumber(List<Account> accounts, Long id) {
        return accounts.stream()
                .filter(account -> id.equals(account.getId()))
                .findFirst()
                .orElseThrow(() -> new AccountNotFoundException("Account not found: " + id));
    }
}
