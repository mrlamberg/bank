package ru.parma.test.service;

import org.springframework.data.domain.Page;
import ru.parma.test.dto.ClientDTO;
import ru.parma.test.entity.Client;
import ru.parma.test.filter.ClientFilter;

import java.math.BigDecimal;

public interface ClientService {

    Client findById(Long clientId);

    Client createAccount(Long clientId, BigDecimal startBalance);

    Client create(ClientDTO clientDTO);

    Page<Client> getClients(ClientFilter filter);
}
