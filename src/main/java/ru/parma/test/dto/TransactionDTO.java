package ru.parma.test.dto;

import org.javamoney.moneta.Money;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class TransactionDTO {

    @NotNull
    @Positive
    private Long fromBankAccount;

    @NotNull
    @Positive
    private Long toBankAccount;

    @NotNull
    @Positive
    private BigDecimal value;

    public Money getMoney() {
        return Money.of(value, "RUB");
    }

    public Long getFromBankAccount() {
        return fromBankAccount;
    }

    public void setFromBankAccount(Long fromBankAccount) {
        this.fromBankAccount = fromBankAccount;
    }

    public Long getToBankAccount() {
        return toBankAccount;
    }

    public void setToBankAccount(Long toBankAccount) {
        this.toBankAccount = toBankAccount;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
