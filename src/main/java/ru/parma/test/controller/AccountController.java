package ru.parma.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.parma.test.dto.AccountDTO;
import ru.parma.test.service.AccountService;

@Controller
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/client/account", method = RequestMethod.POST)
    public String createAccount(Model model, AccountDTO accountDTO) {

        model.addAttribute("client", accountService.create(accountDTO));

        return "client";
    }
}
