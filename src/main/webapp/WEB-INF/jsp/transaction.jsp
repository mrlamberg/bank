<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Провести транзакцию</title>
</head>
<body>
<%@ include file="menu.jsp" %>
<div class="container">
    <div class="card" style="width: 28rem;">
        <div class="card-body">
            <h5 class="card-title">Создание транзакции</h5>
            <form action="transaction" method="post">
                <div class="row">
                    <div class="col">
                        <input type="text" class="form-control" name="fromBankAccount" placeholder="Счет расхода"
                               pattern="[1-9]+[0-9]*" required>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="toBankAccount" placeholder="Счет прихода"
                               pattern="[1-9]+[0-9]*" required>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="value" placeholder="Сумма" pattern="[1-9]+[0-9]*"
                               required>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="currency" placeholder="Валюта" value="RUB"
                               required readonly>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Создать транзакцию</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
