package ru.parma.test.service;

import org.javamoney.moneta.Money;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.parma.test.dto.AccountDTO;
import ru.parma.test.dto.ClientDTO;
import ru.parma.test.entity.Account;
import ru.parma.test.entity.Client;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceTest {

    @Autowired
    private ClientService clientService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TransactionService transactionService;

    public TransactionServiceTest() {
    }

    public Client createAccount() {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName("testCreate");
        clientDTO.setAddress("testCreate");
        Client client = clientService.create(clientDTO);
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setClientId(client.getId());
        accountDTO.setStartBalance(BigDecimal.valueOf(client.getId()));
        return accountService.create(accountDTO);
    }

    @Test
    @Transactional
    public void createTransaction() {
        Account account1Before = this.createAccount().getAccounts().get(0);
        Account account2Before = this.createAccount().getAccounts().get(0);
        BigDecimal account1StartMoney = account1Before.getBalanceAmount();
        BigDecimal account2StartMoney = account2Before.getBalanceAmount();

        transactionService.transferBetweenAccounts(account1Before.getId(), account2Before.getId(), Money.of(1, "RUB"));

        Account account1After = clientService.findById(account1Before.getClient().getId()).getAccounts().get(0);
        Account account2After = clientService.findById(account2Before.getClient().getId()).getAccounts().get(0);

        BigDecimal account1EndMoney = account1After.getBalanceAmount();
        BigDecimal account2EndMoney = account2After.getBalanceAmount();

        Assert.assertEquals(account1StartMoney.add(BigDecimal.valueOf(-1)), account1EndMoney);
        Assert.assertEquals(account2StartMoney.add(BigDecimal.valueOf(1)), account2EndMoney);

    }
}
