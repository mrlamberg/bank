package ru.parma.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.parma.test.dto.ClientDTO;
import ru.parma.test.filter.ClientFilter;
import ru.parma.test.service.ClientService;

import java.util.stream.Collectors;

@Controller
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @RequestMapping(value = {"/", "/clientsList"}, method = RequestMethod.GET)
    public String clientsList(Model model, ClientFilter filter) {

        model.addAttribute("clients", clientService.getClients(filter).stream().collect(Collectors.toList()));

        return "clientsList";
    }

    @RequestMapping(value = "/client", method = RequestMethod.POST)
    public String create(Model model, ClientDTO clientDTO) {

        model.addAttribute("client", clientService.create(clientDTO));

        return "client";
    }

    @RequestMapping(value = "/client/{clientId}", method = RequestMethod.GET)
    public String getClient(Model model, @PathVariable Long clientId) {

        model.addAttribute("client", clientService.findById(clientId));

        return "client";
    }
}
