package ru.parma.test.filter;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class ClientFilter {

    private Integer page;
    private Integer size;

    public ClientFilter() {
    }

    public Integer getPage() {
        if (page == null) {
            return 0;
        } else {
            return page;
        }
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        if (size == null) {
            return 10;
        } else {
            return size;
        }
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public PageRequest getPageRequest() {
        return PageRequest.of(this.getPage(), this.getSize(), Sort.Direction.ASC, "name");
    }
}
