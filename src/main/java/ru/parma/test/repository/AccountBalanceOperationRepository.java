package ru.parma.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.parma.test.entity.AccountBalanceOperation;

public interface AccountBalanceOperationRepository extends JpaRepository<AccountBalanceOperation, Long> {

}
