package ru.parma.test.entity;

import org.springframework.stereotype.Component;

@Component
public class AccountBalanceOperationFactory {

    public AccountBalanceOperation createDebitAccountBalanceOperation(Transaction transaction) {
        return new AccountBalanceOperation(
                transaction,
                transaction.getFromAccount(),
                transaction.getFromAccount().getBalanceAmount(),
                transaction.getAmount().negate()
        );
    }

    public AccountBalanceOperation createCreditAccountBalanceOperation(Transaction transaction) {
        return new AccountBalanceOperation(
                transaction,
                transaction.getToAccount(),
                transaction.getToAccount().getBalanceAmount(),
                transaction.getAmount()
        );
    }
}
