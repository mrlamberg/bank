package ru.parma.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import ru.parma.test.entity.Account;

import javax.persistence.LockModeType;
import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<Account> findByIdIn(List<Long> ids);
}
