package ru.parma.test.service;

import ru.parma.test.dto.AccountDTO;
import ru.parma.test.entity.Client;

public interface AccountService {

    Client create(AccountDTO accountDTO);
}
