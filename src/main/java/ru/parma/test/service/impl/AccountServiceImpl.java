package ru.parma.test.service.impl;

import org.springframework.stereotype.Component;
import ru.parma.test.dto.AccountDTO;
import ru.parma.test.entity.Client;
import ru.parma.test.service.AccountService;
import ru.parma.test.service.ClientService;

import javax.transaction.Transactional;

@Component
public class AccountServiceImpl implements AccountService {

    private final ClientService clientService;

    public AccountServiceImpl(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    @Transactional
    public Client create(AccountDTO accountDTO) {
        return clientService.createAccount(accountDTO.getClientId(), accountDTO.getStartBalance());
    }
}
