package ru.parma.test.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ClientDTO {

    @NotNull
    @Pattern(regexp = "[a-zA-Zа-яА-Я]+")
    private String name;

    @NotNull
    @Pattern(regexp = "[a-zA-Zа-яА-Я0-9, ]+")
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
