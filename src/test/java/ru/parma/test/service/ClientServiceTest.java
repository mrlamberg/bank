package ru.parma.test.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.parma.test.dto.ClientDTO;
import ru.parma.test.entity.Client;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTest {

    @Autowired
    private ClientService clientService;


    public ClientServiceTest() {
    }

    @Test
    public void createClient() {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName("testCreate");
        clientDTO.setAddress("testCreate");
        Client client = clientService.create(clientDTO);
        Assert.assertEquals(clientDTO.getName(), client.getName());
        Assert.assertEquals(clientDTO.getAddress(), client.getAddress());
    }
}
