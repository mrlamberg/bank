package ru.parma.test.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ru.parma.test.dto.ClientDTO;
import ru.parma.test.entity.Account;
import ru.parma.test.entity.Client;
import ru.parma.test.exception.ClientNotFoundException;
import ru.parma.test.filter.ClientFilter;
import ru.parma.test.repository.ClientRepository;
import ru.parma.test.service.ClientService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;

@Component
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Page<Client> getClients(ClientFilter filter) {
        return clientRepository.findAll(filter.getPageRequest());
    }

    @Override
    public Client create(ClientDTO clientDTO) {
        return clientRepository.save(new Client(clientDTO));
    }

    @Override
    public Client findById(Long clientId) {
        try {
            return clientRepository.getOne(clientId);
        } catch (EntityNotFoundException e) {
            throw new ClientNotFoundException("Client not found" + clientId);
        }
    }

    @Override
    @Transactional
    public Client createAccount(Long clientId, BigDecimal startBalance) {
        Client client = clientRepository.getOne(clientId);
        if (client.getAccounts() == null) {
            client.setAccounts(new ArrayList<>());
        }
        client.getAccounts().add(new Account(client, startBalance));
        return clientRepository.save(client);
    }
}
