package ru.parma.test.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.javamoney.moneta.Money;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_account_id", nullable = false)
    private Account fromAccount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_account_id", nullable = false)
    private Account toAccount;

    @Columns(columns = {@Column(name = "currency"), @Column(name = "amount")})
    @Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money amount;

    @CreationTimestamp
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    public Transaction() {
    }

    Transaction(Account fromAccount, Account toAccount, Money amount) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonProperty("from_bank_account")
    public Account getFromAccount() {
        return fromAccount;
    }

    @JsonProperty("to_account")
    public Account getToAccount() {
        return toAccount;
    }

    @JsonProperty
    public Money getAmount() {
        return amount;
    }

    @JsonProperty("create_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public LocalDateTime getCreateTime() {
        return createTime;
    }
}
