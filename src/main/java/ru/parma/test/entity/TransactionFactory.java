package ru.parma.test.entity;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;

@Component
public class TransactionFactory {

    public Transaction createTransaction(Account fromBankAccount, Account toBankAccount, Money amount) {
        return new Transaction(fromBankAccount, toBankAccount, amount);
    }
}
