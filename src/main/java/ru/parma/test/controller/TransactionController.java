package ru.parma.test.controller;

import com.fasterxml.jackson.databind.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.parma.test.dto.TransactionDTO;
import ru.parma.test.entity.Transaction;
import ru.parma.test.exception.InvalidDateException;
import ru.parma.test.filter.TransactionFilter;
import ru.parma.test.service.TransactionService;

import java.text.ParseException;

@Controller
@ExposesResourceFor(Transaction.class)
public class TransactionController {

    private final TransactionService transactionService;
    private final Module module;

    @Autowired
    public TransactionController(TransactionService transactionService, @Qualifier("moneyModule") Module module) {
        this.transactionService = transactionService;
        this.module = module;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public String transfer(Model model, @Validated TransactionDTO transactionDTO) {

        transactionService.transferBetweenAccounts(
                transactionDTO.getFromBankAccount(),
                transactionDTO.getToBankAccount(),
                transactionDTO.getMoney()
        );

        return "transaction";
    }

    @RequestMapping(value = "/transactionsList", method = RequestMethod.GET)
    public String transactionsList(Model model, TransactionFilter filter) {
        try {
            model.addAttribute("transactions", transactionService.find(filter));
        } catch (ParseException e) {
            throw new InvalidDateException("wrong date format");
        }
        return "transactionsList";
    }

    @RequestMapping(value = "/transaction", method = RequestMethod.GET)
    public String transaction() {
        return "transaction";
    }
}
