<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Список клиентов</title>
</head>
<body>
<%@ include file="menu.jsp" %>
<div class="container">
    <div class="card" style="width: 28rem;">
        <div class="card-body">
            <h5 class="card-title">Создание клиента</h5>
            <form action="client" method="post">
                <div class="row">
                    <div class="col">
                        <input type="text" class="form-control" name="name" placeholder="Имя клиента"
                               pattern="[a-zA-Zа-яА-Я]+" required>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="address" placeholder="Адрес клиента"
                               pattern="[a-zA-Zа-яА-Я0-9, ]+" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Создать клиента</button>
            </form>
        </div>
    </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Имя клиента</th>
            <th>Адрес клиента</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${clients}" var="client">
            <tr onclick="window.location.href='/client/${client.id}'; return false">
                <td>${client.name}</td>
                <td>${client.address}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <nav>
        <ul class="pagination">
            <%
                int pageNumber = 0;
                if (request.getParameter("page") != null)
                    pageNumber = Integer.parseInt(request.getParameter("page"));

                if (pageNumber != 0) {
                    out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"clientsList?page=" + (pageNumber - 1) + "\">Previous</a></li>");
                }
                out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"clientsList?page=" + (pageNumber + 1) + "\">Next</a></li>");
            %>
        </ul>
    </nav>
</div>
</body>
</html>
