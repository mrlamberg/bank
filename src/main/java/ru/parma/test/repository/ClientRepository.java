package ru.parma.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.parma.test.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
