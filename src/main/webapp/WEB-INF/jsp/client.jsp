<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Клиент</title>
</head>
<body>
<%@ include file="menu.jsp" %>
<div class="card" style="width: 28rem;">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    ${client.name}
                </div>
                <div class="col-sm">
                    ${client.address}
                </div>
            </div>
        </div>
        <form action="account" method="post">
            <input class="form-control" type="text" name="clientId" value="${client.id}" style="display: none">
            <input class="form-control" type="text" name="startBalance" placeholder="Стартовый баланс" pattern="[0-9]+"
                   required>
            <button type="submit" class="btn btn-primary">Создать счет</button>
        </form>
    </div>
</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th>Номер счета</th>
        <th>Баланс счета</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${client.accounts}" var="account">
        <tr>
            <td>${account.id}</td>
            <td>${account.balance.number.doubleValue()} ${account.balance.currency}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
