package ru.parma.test.service;

import org.javamoney.moneta.Money;
import ru.parma.test.entity.AccountBalanceOperation;
import ru.parma.test.entity.Transaction;
import ru.parma.test.filter.TransactionFilter;

import java.text.ParseException;
import java.util.List;

public interface TransactionService {

    Transaction transferBetweenAccounts(Long fromAccountId, Long toAccountId, Money amount);

    List<AccountBalanceOperation> find(TransactionFilter filter) throws ParseException;
}
