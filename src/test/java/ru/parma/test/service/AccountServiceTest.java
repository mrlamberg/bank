package ru.parma.test.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.parma.test.dto.AccountDTO;
import ru.parma.test.dto.ClientDTO;
import ru.parma.test.entity.Client;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    private ClientService clientService;

    @Autowired
    private AccountService accountService;

    public AccountServiceTest() {
    }

    @Test
    public void createAccount() {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName("testCreate");
        clientDTO.setAddress("testCreate");
        Client client = clientService.create(clientDTO);
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setClientId(client.getId());
        accountDTO.setStartBalance(BigDecimal.valueOf(client.getId()));
        Client client1 = accountService.create(accountDTO);
        Assert.assertEquals(client1.getId(), client.getId());
    }
}
