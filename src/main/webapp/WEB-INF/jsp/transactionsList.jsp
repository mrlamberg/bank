<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Список транзакций</title>
</head>
<body>
<%@ include file="menu.jsp" %>
<div class="container">
    <div class="card" style="width: 28rem;">
        <div class="card-body">
            <h5 class="card-title">Фильтрация</h5>
            <form action="transactionsList" method="get">
                <div class="row">
                    <div class="col">
                        <input type="date" class="form-control" name="dateFrom" placeholder="Начало периода">
                    </div>
                    <div class="col">
                        <input type="date" class="form-control" name="dateTo" placeholder="Конец периода">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="name" placeholder="Имя клиента"
                               pattern="[a-zA-Zа-яА-Я]+">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Применить фильтр</button>
            </form>
        </div>
    </div>
    <table>
        <tr>
            <td>Дата транзакции</td>
            <td>Номер счета</td>
            <td>Сумма транзакции</td>
        </tr>
        <c:forEach items="${transactions}" var="transaction">
            <tr>
                <td>${transaction.createTime}</td>
                <td>${transaction.account.id}</td>
                <td>${transaction.amount.number.doubleValue()} ${transaction.amount.currency}</td>
            </tr>
        </c:forEach>
    </table>
    <nav>
        <ul class="pagination">
            <%
                int pageNumber = 0;
                if (request.getParameter("page") != null)
                    pageNumber = Integer.parseInt(request.getParameter("page"));
                String url = request.getQueryString();
                if (url != null) {
                    url = url.replaceAll("(page=)[0-9]*[&]?", "");
                    if (url != null && !url.isEmpty()) {
                        url = "&" + url;
                    }
                } else {
                    url = "";
                }

                if (pageNumber != 0) {
                    out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"transactionsList?page=" + (pageNumber - 1) + url + "\">Previous</a></li>");
                }
                out.println("<li class=\"page-item\"><a class=\"page-link\" href=\"transactionsList?page=" + (pageNumber + 1) + url + "\">Next</a></li>");
            %>
        </ul>
    </nav>
</div>
</body>
</html>

