package ru.parma.test.filter;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

public class TransactionFilter {

    private static SimpleDateFormat simpleDateFormat;

    static {
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }

    private String dateFrom;
    private String dateTo;
    private String name;
    private Integer page;
    private Integer size;

    public TransactionFilter() {
    }

    public LocalDateTime getDateFrom() throws ParseException {
        if (dateFrom == null) {
            return null;
        }
        return new Timestamp(simpleDateFormat.parse(dateFrom).getTime()).toLocalDateTime();
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDateTime getDateTo() throws ParseException {
        if (dateTo == null) {
            return null;
        }
        return new Timestamp(simpleDateFormat.parse(dateTo).getTime()).toLocalDateTime().plusDays(1).minusNanos(1);
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPage() {
        if (page == null) {
            return 0;
        } else {
            return page;
        }
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        if (size == null) {
            return 10;
        } else {
            return size;
        }
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
